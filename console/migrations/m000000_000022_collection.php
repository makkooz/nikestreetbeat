<?php

use yii\db\Schema;

/**
 * Class m000000_000022_collection
 *
 * @author Pavel Pantyukhov <pavel.p@initflow.com>
 * @since 0.1.0
 */
class m000000_000022_collection extends \yii\db\Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%collection}}', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%collection}}');
    }
}

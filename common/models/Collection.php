<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%collection}}".
 *
 * @property integer $id
 * @property string $title
 *
 * @property Collection[] $collections
 *
 * @author Pantyukhov Pavel <13pavel.p@initflow.com>
 * @since 0.1.0
 */
class Collection extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%collection}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('writesdown', 'ID'),
            'title' => Yii::t('writesdown', 'Title'),
        ];
    }



}
